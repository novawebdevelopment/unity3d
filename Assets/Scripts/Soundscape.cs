//
//  Soundscape.cs
//
//  Author:
//       Kevin Cole <kevin.cole@novawebdevelopment.org>
//
//  Copyright (c) 2017 Kevin Cole CC-BY-SA (C) 2017
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

// Why make it simple?
//
// http://answers.unity3d.com/questions/279750/loading-data-from-a-txt-file-c.html
// https://docs.unity3d.com/ScriptReference/JsonUtility.FromJson.html
//

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Text;
using System;       // Provides Console (at least)

[System.Serializable]
public class Soundscape {
    public string description;

    public static Soundscape CreateFromJSON() {
        string jsonBuffer;

        // Determine user's home directory for the current OS

        string homePath;
        PlatformID platform = Environment.OSVersion.Platform;

        switch (platform) {
        case PlatformID.Unix:
        case PlatformID.MacOSX:
            homePath = Environment.GetEnvironmentVariable("HOME");
            break;
        case PlatformID.Win32NT:
        case PlatformID.Win32S:
        case PlatformID.Win32Windows:
        case PlatformID.WinCE:
            homePath = Environment.GetEnvironmentVariable("%HOMEDRIVE%%HOMEPATH%");
            break;
        default:
            homePath = "";
            Console.WriteLine("ERROR: Unrecognized operating system !!!");
            break;
        }

        // Establish paths to important configurations and results

        string presetsPath = homePath + "/.config/sound-advice/presets";
        string ratingsPath = homePath + "/.config/sound-advice/ratings";
        string resultsPath = homePath + "/Data/Sound Advice";
        string[] paths     = new string[] {presetsPath, ratingsPath, resultsPath};

        // Create directories if necessary

        foreach (string path in paths) {
            if (!Directory.Exists(path)) {
                Directory.CreateDirectory(path);
                Console.WriteLine("{0} created.", path);
            }
            else {
                Console.WriteLine("{0} exists", path);
            }
        }

        // For a dummy test, just point to one JSON file

        string fileName    = presetsPath + "/iconic/00-index.json";       // DUMMY

        Console.WriteLine("[Sounscape CreateFromJSON active]\n");         // DEBUG
        try {
            StreamReader theReader = new StreamReader(fileName,
                    Encoding.Default);

            // Immediately clean up the reader after this block of code is done.
            // You generally use the "using" statement for potentially memory-
            // intensive objects instead of relying on garbage collection.
            //
            // (Do not confuse this with the using directive
            // for namespace at the  beginning of a class !!!)
            //

            using (theReader) {
                jsonBuffer = theReader.ReadToEnd();
                theReader.Close(); // Done reading, close the reader
                Console.WriteLine("{0}\n", jsonBuffer);                       // DEBUG
            }
        }

        // If anything broke in the try block,
        // throw an exception with information on what didn't work
        //

        catch(Exception e) {
            Console.WriteLine("{0}\n", e.Message);  // ERROR!
            jsonBuffer = "{\"description\": \"ERROR!\"}";
        }
        return JsonUtility.FromJson<Soundscape>(jsonBuffer);
    }
}
